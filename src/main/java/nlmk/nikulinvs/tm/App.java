package nlmk.nikulinvs.tm;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.sql.SQLOutput;
import java.util.Scanner;

import static nlmk.nikulinvs.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */

public class App {

    public static void main(final String[] args) {
        displayWelcom();
        run(args);

        final Scanner scaner = new Scanner(System.in);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scaner.nextLine();
            run(command);
        }
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
        }

    private static int run(final String param) {
        if (param == null) return -1;
        switch (param) {
            case CMD_VERSION: return displayVersion();
            case CMD_ABOUT: return displayAbout();
            case CMD_HELP: return displayHelp();
            default: return displayError();
            case CMD_EXIT: return displayExit();
            }
    }
    private static int displayExit() {
        System.out.println("Terminate programm ...");
        return 0;
    }

    private static int displayError() {
        System.out.println("Error! Unknown programm arguments ...");
        return -1;
    }

    private static void displayWelcom() {
        System.out.println("**Welcom to Task Manager**");
        }

    private static int displayHelp() {
        System.out.println("version - Display programm version");
        System.out.println("about - Display developer info");
        System.out.println("help - Display list of terminal commands");
        System.out.println("exit - Terminate console application");
        return 0;
        }

    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
        }

    private static int displayAbout() {
        System.out.println("Nikulin Viktor");
        System.out.println("nikulin_vs@nlmk.com");
        return 0;
        }

}
